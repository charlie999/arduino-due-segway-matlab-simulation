function [AngleEst] = AngleKalman (AccelAngle, GyroRate)

dt = 0.01;

persistent X;
if isempty(X)
    X = [0, 0]';
end
persistent P;
Sz = 0.003;
Sw = [0.001 0; 0 0.003];
P = [Sw(1, 1) 0; 0 Sw(2, 2)];

A = [1 -dt; 0 1];
B = [dt 0]';
C = [1 0];

X_temp = A * X + B * GyroRate;
Inn = AccelAngle - C * X_temp;
s = C * P * C' + Sz;
K = A * P * C' / s;
X_temp = X_temp + K * Inn;
P = A * P * A' - K * C * P * A' + Sw;

X = X_temp;

AngleEst = X(1);