function [THETA_dot2, X_dot2] = balancingRobot (F, THETA, THETA_dot)


THETA_dot2 = (9.81 * sin (THETA) + cos (THETA) * (-F - Mrc * l * sin (THETA) * (THETA_dot^2)) / (Mwh + Mrc));
THETA_dot2 = THETA_dot2 / (l * ((4/3) - (Mrc * THETA * cos (THETA)^2) / (Mwh + Mrc)));
X_dot2 = (F + Mrc * l * ((THETA_dot ^ 2)* sin (THETA) - THETA_dot2 * cos (THETA))) / (Mwh + Mrc);